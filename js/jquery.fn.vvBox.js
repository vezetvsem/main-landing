(function($) {
    $.fn.extend({
        vvLoginBox: function(settings) {
            return this.each(function() {
                $(".x-open-login-box").on('click', function(e) {
                    e.preventDefault();
                    showLoginBox();
                });

                $('#x-login-dialog .close').on('click', function(e) {
                    e.preventDefault();
                    hideLoginBox();
                });

                $(document).on('click', '.x-backdrop', function(e) {
                    e.preventDefault();
                    hideLoginBox();
                });

            });
        }
    });

    function showLoginBox() {
        var $div = $('<div></div>');
        $div.addClass('x-backdrop modal-backdrop fade');

        $('body').append($div);

        $('#x-login-dialog').removeClass('hidden');
        setTimeout(function() {
            $('.x-backdrop').addClass('in');
            $('#x-login-dialog').addClass('in');
        }, 10);
    }

    function hideLoginBox() {

        setTimeout(function() {
            $('#x-login-dialog').removeClass('in');
            $('.x-backdrop').removeClass('in');
            setTimeout(function() {
                $('.x-backdrop').addClass('hidden');
                $('.x-backdrop').remove();
                $('#x-login-dialog').addClass('hidden');
            }, 100);
        }, 10);
    }

})(jQuery);