var SCROLL_TRESHOLD = 30;
var FIXED_CLASS = '_fixed';
var HEADER_HEIGHT = 88;
var SCROLL_TIME = 500;

var documentElement = $(document);
var headerElement = $('.x-header');

documentElement.on('scroll', function () {
    var scrollPosition = documentElement.scrollTop();
    headerElement.toggleClass(FIXED_CLASS, scrollPosition > SCROLL_TRESHOLD);
});

$(function() {
    var dataOld = 'flat';
    $('.x-description-item').on('click', function(){
        var dataNew = $(this).data('type');
        if (dataNew != dataOld){
        	$(this).addClass('_active');
        	$('.x-description-item[data-type ='+dataOld+']').removeClass('_active');
            $('.x-description-block[data-type ='+dataOld+']').addClass('_hidden');
            $('.x-description-block[data-type ='+dataNew+']').removeClass('_hidden');
            dataOld = dataNew;
        }
    });

    $('.x-arrowdown').on('click', function()
    {
    	$('html, body').animate({
    		scrollTop: $('#main').offset().top-HEADER_HEIGHT
    	}, SCROLL_TIME);
    });
    
});

$(function() {
    var bodyElement = $('body');
    var isDrawerOpened = false;
    var disableScroll = function(e) {
        if (e.target.id == 'el') return;
        e.preventDefault();
        e.stopPropagation();
    }

    $('.x-header-control').on('click', function () {
        isDrawerOpened = !isDrawerOpened;
        headerElement.toggleClass('_collapsed', !isDrawerOpened);
        if (isDrawerOpened) {
            bodyElement.on('mousewheel touchmove', disableScroll);
        } else {
            bodyElement.off('mousewheel touchmove', disableScroll);
        }
    });
});

$('#x-login-dialog').vvLoginBox();
$('#x-support-dialog').vvWidgetSupport();
