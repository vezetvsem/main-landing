(function($) {
    $.fn.extend({
        vvWidgetSupport: function() {
            return this.each(function() {

                var $self = this;

                $("header .contact").click(function(e) {
                    e.preventDefault();
                    showBox.call($self);
                });

                $('body').on('click', ".x-user-voice", function(e){
                    e.preventDefault();
                    showBox.call($self);
                });

                $('.close', this).click(function(e) {
                    e.preventDefault();
                    hideBox.call($self);
                });

                $(document).on('click', '.x-backdrop', function(e) {
                    e.preventDefault();
                    hideBox.call($self);
                });

                $('.x-support-form').on('submit', function() {
                    sendMessage.call($self);
                });

            });
        }
    });

    function sendMessage()
    {
        var $self = this;

        $.ajax({
            url: '/widget/support',
            data: {
                'subject': $('.x-subject', $self).val(),
                'message': $('.x-message', $self).val(),
                'email': $('.x-email', $self).val()
            },
            type: 'post',
            dataType: 'JSON',
            success: function(data) {

                if (data.success) {
                    $('.x-warning-message', $self).addClass('hidden');

                    $('.x-success-message', $self).html(data.message);
                    $('.x-success-message', $self).removeClass('hidden');
                } else {
                    $('.x-warning-message', $self).html(data.message);
                    $('.x-warning-message', $self).removeClass('hidden');
                }

            }
        });
    }

    function showBox() {
        var $self = this;

        var $div = $('<div></div>');
        $div.addClass('x-backdrop modal-backdrop fade');

        $('body').append($div);

        $($self).removeClass('hidden');
        setTimeout(function() {
            $('.x-backdrop').addClass('in');
            $($self).addClass('in');
        }, 10);
    }

    function hideBox()
    {
        var $self = this;

        setTimeout(function() {
            $($self).removeClass('in');
            $('.x-backdrop').removeClass('in');
            setTimeout(function() {
                $('.x-backdrop').addClass('hidden');
                $('.x-backdrop').remove();
                $($self).addClass('hidden');
            }, 100);
        }, 10);
    }

})(jQuery);