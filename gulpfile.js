var gulp = require('gulp');
var concat = require('gulp-concat');
var postcss = require('gulp-postcss');
var del = require('del');
var autoprefixer = require('autoprefixer-core');
var cssnext = require('cssnext');
var nested = require('postcss-nested');

var browserSync = require('browser-sync').create();

gulp.task('css', function () {
    var processors = [
        cssnext({browsers: '> 1%, last 2 versions, Firefox ESR, Opera 12.1, IE 8'}),
        nested
    ];

    gulp.src('css/vendor/bootstrap.min.css')
        .pipe(gulp.dest('./build'));

    gulp.src(['css/main.css'])
        .pipe(postcss(processors))
        .pipe(concat('index.css'))
        .pipe(gulp.dest('build'));
});

gulp.task('sync', ['css'], function () {
    browserSync.init({
        files: ['build/*.*'],
        server: {
            baseDir: './'
        },
        open: false
    });

    gulp.watch('css/**/*.css', ['css']);
    gulp.watch('index.html').on('change', browserSync.reload);
});

gulp.task('clean', function () {
    del('build');
});

gulp.task('default', ['css']);
